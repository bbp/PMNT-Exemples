jQuery(document).ready(function() {
    var origine = [49.11848, 6.17502];
    
    jQuery(".map_conteneur").PMNT_map({
        onClick: testFunc,
        tile_layer: "http://{s}.tile.cloudmade.com/830c09c48ab0486396fb847417a3c66f/22677/256/{z}/{x}/{y}.png",
        zoom_position: "topright",
        initial_coordinates: [origine[0], origine[1]],
        initial_zoom: 18
    });
    
    var myIcon = L.icon({
        iconUrl: './icon_home.png',
        iconSize: [23, 26],
        iconAnchor: [9, 26],
        popupAnchor: [3, -26],
    });

    /* 
    Ajout de plusieurs markers via un fichier geojson : 
        - src est un fichier json des 'points' comportant latitude et longitude et 
        toute info complémentaire sous un layer ( activable/desactivable : visible/non visible avec appui sur un case a cocher ), 
        - icon est un icon au choix
    */
    map.addGeoPoints({
        src: "./events.json",
        icon: myIcon
        });
        
    /*
    Ajout d'un tracé décrit dans un fichier geojson, par exemple un tracé de randonnée 
    */
    map.addGeoNetwork({
        src: "./test.json"
    });
})
jQuery(document).ready(function() {
    var origine = [49.11848, 6.17502];

    jQuery(".map_conteneur").PMNT_map({
        tile_layer: "http://{s}.tile.cloudmade.com/830c09c48ab0486396fb847417a3c66f/22677/256/{z}/{x}/{y}.png",
        zoom_position: "topright",
        initial_coordinates: [origine[0], origine[1]],
        initial_zoom: 18
    });

    var value = {
        nom: "test",
        latitude: 49,
        longitude: 6,
        content: "lorem ipsum et tutti quanti"
    }
    // Exemple d ajout d une popup à un endroit spécifique de la map, à l arrivée sur la page 
    jQuery(".map_conteneur").PMNT_map.popup({
        latitude: value.latitude,
        longitude: value.longitude,
        content: "<h3 style='color:gray'>" + value.content + "</h3>",
        zoom: 17
    })
})
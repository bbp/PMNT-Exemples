# PMNT-Exemples
Vous trouverez ici des fichiers d'exemples simples d'utilisation du module de cartographie « PMNT ».
L'idée générale est de faciliter le plus possible la création d'une carte et l'ajout d'informations par dessus.

# Exemples
Il y a actuellement 4 exemples :
- [création de la carte](https://gitlab.com/bbp/PMNT-Exemples/blob/master/1_creation_dune_carte.js)
- [création d'une popup](https://gitlab.com/bbp/PMNT-Exemples/blob/master/2_creation_dune_popup.js)
- [création d'un marker](https://gitlab.com/bbp/PMNT-Exemples/blob/master/3_creation_dun_marker.js)
- [import de donnes GeoJson](https://gitlab.com/bbp/PMNT-Exemples/blob/master/4_ajout_donnees_geojson.js)
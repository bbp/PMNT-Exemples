jQuery(document).ready(function() {
    var origine = [49.11848, 6.17502];

    jQuery(".map_conteneur").PMNT_map({
        onClick: testFunc,
        tile_layer: "http://{s}.tile.cloudmade.com/830c09c48ab0486396fb847417a3c66f/22677/256/{z}/{x}/{y}.png",
        zoom_position: "topright",
        initial_coordinates: [origine[0], origine[1]],
        initial_zoom: 18
    });

    var value = {
        nom: "test",
        latitude: 49,
        longitude: 6,
        content: "lorem ipsum et tutti quanti"
    }
    var myIcon = L.icon({
        iconUrl: './icon_home.png',
        iconSize: [23, 26],
        iconAnchor: [9, 26],
        popupAnchor: [3, -26],
    });
    
    // Ajout d un marker ou plusieurs markers => markers est un tableau JS
    map.addMarker({
        markers: [{
            latitude: value.latitude,
            longitude: value.longitude,
            icon: myIcon,
            click: '<h3 class="popup_marker">' + value.nom + '</h3>'
        }]
    });
})